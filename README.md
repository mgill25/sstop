SSTOP: Stop Stating the Obvious, (Punk)
========================================

A simple, fun app I created playing around with Node for the first time. Uses [Express](expressjs.com) and [socket.io](socket.io).

Works by having a presenter tab open, and then, the audience can connect. The audience can then 'alert' the presenter, if it ever gets boring. We actually used it. ;)

