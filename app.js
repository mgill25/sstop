// SSTOP - Stop saying the obvious, punk!
    
var express = require('express')
    , app = express()
    , http = require('http')
    , server = http.createServer(app)
    , socket = require('socket.io')
    , io = socket.listen(server)
    , path = require('path');


// routing
app.get('/', function(req, res){
    console.log('Req header: ' + req.header('host'));
    console.log('Req: ' + req.params);
    console.log('Res: ' + res);
    res.sendfile(__dirname + '/index.html');
});

app.get('/presenter', function(req, res){
    res.sendfile(__dirname + '/presenter.html');
});

app.get('/audience', function(req, res){
    res.sendfile(__dirname + '/audience.html');
});

app.use(express.static(__dirname + '/static'));

var presenter = {};
var audience = {};
var userCount = 0;

io.sockets.on('connection', function(socket){
    socket.on('adduser', function(userId){
        // console.log('HELLO WORLD');
        socket.userId = userId;
        if (userId == 'presenter'){
            // redirect to the page where the audio file loads and wait for the
            // audience to press the play button.
            presenter[userId] = userId;
            // socket.broadcast.emit('AddPresenter', 'SERVER', userId + ' is the presenter');
            console.log('Presenter has arrived!');
        }
        else if (userId == 'audience'){
            // redirect to the page with a button, which when clicked, triggers
            // the audio file to play on the presenter's browser.
            audience[userId] = userId;
            // socket.broadcast.emit('AddPresenter', 'SERVER', userId + ' is an audience member!');
            console.log('Another audience member is here. ^_^');
            userCount += 1;
            console.log(userCount);
            // io.sockets.emit('userCount', userCount);
            io.sockets.emit('updateusers', userCount);
        }
    });

    socket.on('disconnect', function(userId){
        // remove the user from the global list
        if (userCount){
            userCount -= 1;
            io.sockets.emit('updateusers', userCount);
        }

        if (userId == 'audience'){
            delete audience[userId];
        }
        else if(userId == 'presenter'){
           delete presenter[userId]; 
        }
    });

    // listener for the play event from the client (audience)
    socket.on('play', function(play_sound){
        // When a play event is emitted from a client, 
        // emit an audio event.
        if (play_sound == true){
            console.log("Recieved a play event!");
            io.sockets.emit('audio');
            console.log("Event Emitted!");
        }
                  
    }); 
});

server.listen(3000, function(){
    console.log("Express server listening on port 3000");
});

